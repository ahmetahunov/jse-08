https://gitlab.com/ahmetahunov/jse-08
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven 4.0

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/jse-08.git
cd jse-08
mvn clean install
```

## run app
```bash
java -jar target/release/bin/taskmanager.jar
```
