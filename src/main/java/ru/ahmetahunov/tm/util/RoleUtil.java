package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.enumerated.Role;

public final class RoleUtil {

    public static @Nullable Role getRole(@NotNull final String role) {
        if ("user".equals(role.toLowerCase()))
            return Role.USER;
        if ("administrator".equals(role.toLowerCase()))
            return Role.ADMINISTRATOR;
        return null;
    }

}
