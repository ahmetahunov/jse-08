package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    @NotNull
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static @NotNull Date parseDate(@NotNull final String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    public static @NotNull String formatDate(@NotNull final Date date) {
        return dateFormatter.format(date);
    }

}
