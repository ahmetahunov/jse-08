package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.entity.Project;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        for (@NotNull Project project : findAll(userId)) {
            remove(project.getId());
        }
    }

    @Override
    public @NotNull List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> projects = new LinkedList<>();
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId)) projects.add(project);
        }
        return projects;
    }

    @Override
    public @Nullable Project findOne(@NotNull final String projectName, @NotNull final String userId) {
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId) && project.getName().equals(projectName))
                return project;
        }
        return null;
    }

    @Override
    public boolean contains(@NotNull final String projectName, @NotNull final String userId) {
        return findOne(projectName, userId) != null;
    }

}
