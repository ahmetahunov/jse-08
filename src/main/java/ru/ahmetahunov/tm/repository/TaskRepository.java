package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.entity.Task;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        for (@NotNull Task task : findAll(userId))
            remove(task.getId());
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId)) tasks.add(task);
        }
        return tasks;
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull final String projectId, @NotNull final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    @Override
    public @Nullable Task findOne(@NotNull final String taskName, @NotNull final String projectId,
                        @NotNull final String userId) {
        for (@NotNull Task task : findAll()) {
            if (task.getUserId().equals(userId)
                    && task.getProjectId().equals(projectId)
                    && task.getName().equals(taskName))
                return task;
        }
        return null;
    }

    @Override
    public boolean contains(@NotNull final String taskName, @NotNull final String projectName,
                            @NotNull final String userId) {
        return findOne(taskName, projectName, userId) != null;
    }

}
