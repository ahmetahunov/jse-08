package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.DateUtil;

@NoArgsConstructor
public final class ProjectEditDatesCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull String getName() {
        return "project-edit-dates";
    }

    @Override
    public @NotNull String getDescription() {
        return "Edit start and finish dates in selected project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DATES]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectName, user.getId());
        if (project == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        @NotNull final String startDate = terminalService.getAnswer("Please enter new start date: ");
        @NotNull final String finishDate = terminalService.getAnswer("Please enter new finish date: ");
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getProjectService().merge(project);
        System.out.println("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
