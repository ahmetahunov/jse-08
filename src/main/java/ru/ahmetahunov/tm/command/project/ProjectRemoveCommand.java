package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "project-remove";
    }

    @Override
    public @NotNull String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[PROJECT REMOVE]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project removedProject = projectService.findOne(projectName, user.getId());
        if (removedProject == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        taskService.removeAllProjectTasks(removedProject.getId(), user.getId());
        projectService.remove(removedProject);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}