package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectSelectCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "project-select";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show selected project with tasks.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[PROJECT SELECT]");
        @NotNull final String name = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project project = projectService.findOne(name, user.getId());
        if (project == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        terminalService.writeMessage(project.getName() + ":");
        int i = 1;
        for (@NotNull Task task : taskService.findAll(project.getId(), user.getId())) {
            terminalService.writeMessage(String.format("  %d. %s", i++, task.getName()));
        }
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
