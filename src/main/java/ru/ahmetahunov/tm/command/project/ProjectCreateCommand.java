package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "project-create";
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECT CREATE]");
        @Nullable final Project project = createNewProject(user.getId());
        if (project == null) terminalService.writeMessage("Already exists.\n[FAILED]");
        else terminalService.writeMessage("[OK]");
    }

    private Project createNewProject(@NotNull final String userId) throws IOException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String name = terminalService.getAnswer("Please enter project name: ");
        if (name.isEmpty()) {
            terminalService.writeMessage("Name cannot be empty.");
            return null;
        }
        @Nullable final Project project = serviceLocator.getProjectService().createNewProject(name, userId);
        if (project == null) return null;
        @NotNull final String description = terminalService.getAnswer("Please enter description:");
        project.setDescription(description);
        @NotNull final String startDate = terminalService.getAnswer("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(startDate));
        @NotNull final String finishDate = terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(finishDate));
        return project;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}