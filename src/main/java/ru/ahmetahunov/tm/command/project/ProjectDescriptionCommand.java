package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "project-description";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show selected project information.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[PROJECT-DESCRIPTION]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project project = projectService.findOne(projectName, user.getId());
        if (project == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        terminalService.writeMessage(InfoUtil.getItemInfo(project));
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
