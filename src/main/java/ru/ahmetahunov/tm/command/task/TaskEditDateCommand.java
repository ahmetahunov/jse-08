package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.DateUtil;

@NoArgsConstructor
public final class TaskEditDateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull String getName() {
        return "task-edit-dates";
    }

    @Override
    public @NotNull String getDescription() {
        return "Edit start and finish dates of selected task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DATES]");
        @NotNull final String projectName = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project project = serviceLocator.getProjectService().findOne(projectName, user.getId());
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        @NotNull final String projectId = (project == null) ? "" : project.getId();
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskName, projectId, user.getId());
        if (task == null) {
            terminalService.writeMessage("Selected task does not exist.");
            return;
        }
        @NotNull final String startDate = terminalService.getAnswer("Please enter new start date: ");
        @NotNull final String finishDate = terminalService.getAnswer("Please enter new finish date: ");
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        serviceLocator.getTaskService().merge(task);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
