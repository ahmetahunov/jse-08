package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

import java.io.IOException;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "task-remove";
    }

    @Override
    public @NotNull String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK REMOVE]");
        @NotNull final String projectName = terminalService.getAnswer("Enter project name or press enter to skip:");
        @Nullable final Project project = projectService.findOne(projectName, user.getId());
        @NotNull final String projectId = (project == null) ? "" : project.getId();
        @NotNull final String name = terminalService.getAnswer("Enter task name: ");
        @Nullable final Task task = taskService.findOne(name, projectId, user.getId());
        if (task == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        taskService.remove(task);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}