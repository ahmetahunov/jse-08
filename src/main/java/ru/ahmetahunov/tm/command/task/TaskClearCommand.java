package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "task-clear";
    }

    @Override
    public @NotNull String getDescription() {
        return "Remove all user's available tasks.";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeAll(user.getId());
        serviceLocator.getTerminalService().writeMessage("[ALL TASKS REMOVED]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}