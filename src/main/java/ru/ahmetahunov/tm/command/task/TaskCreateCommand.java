package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "task-create";
    }

    @Override
    public @NotNull String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK CREATE]");
        @Nullable final Project project = getProject(user.getId());
        @NotNull final String projectId = (project == null) ? "" : project.getId();
        @Nullable final Task task = createNewTask(projectId, user.getId());
        if (task == null) terminalService.writeMessage("Already exists.\n[FAILED]");
        else terminalService.writeMessage("[OK]");
    }

    private Task createNewTask(@NotNull final String projectId, @NotNull final String userId) throws IOException {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String name = terminalService.getAnswer("Please enter task name: ");
        if (name.isEmpty()) {
            terminalService.writeMessage("Name cannot be empty.");
            return null;
        }
        @Nullable final Task task = serviceLocator.getTaskService().createNewTask(name, projectId, userId);
        if (task == null) return null;
        @NotNull final String description = terminalService.getAnswer("Please enter description: ");
        task.setDescription(description);
        @NotNull final String startDate = terminalService.getAnswer("Please enter start date(example: 01.01.2020): ");
        task.setStartDate(DateUtil.parseDate(startDate));
        @NotNull final String finishDate = terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        task.setFinishDate(DateUtil.parseDate(finishDate));
        return task;
    }

    private Project getProject(@NotNull final String userId) throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String name = terminalService.getAnswer("Enter project name or press enter to skip: ");
        @Nullable final Project project = projectService.findOne(name, userId);
        if (project != null) return project;
        terminalService.writeMessage(name + " is not available.");
        @NotNull final String answer = terminalService.getAnswer("Do you want use another project?<y/n>: ");
        if ("y".equals(answer))
            return getProject(userId);
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}