package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import java.io.IOException;

@NoArgsConstructor
public final class TaskMoveCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "task-move";
    }

    @Override
    public @NotNull String getDescription() {
        return "Change project for task.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        terminalService.writeMessage("[TASK-MOVE]");
        @NotNull String projectName = terminalService.getAnswer("Please enter project name: ");
        @Nullable Project project = projectService.findOne(projectName, user.getId());
        @NotNull final String projectId = (project == null) ? "" : project.getId();
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        @Nullable final Task task = taskService.findOne(taskName, projectId, user.getId());
        if (task == null) {
            terminalService.writeMessage("Selected task does not exist.");
            return;
        }
        projectName = terminalService.getAnswer("Please enter new project name: ");
        project = projectService.findOne(projectName, user.getId());
        if (project == null) {
            terminalService.writeMessage("Selected project does not exist.");
            return;
        }
        task.setProjectId(project.getId());
        taskService.merge(task);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
