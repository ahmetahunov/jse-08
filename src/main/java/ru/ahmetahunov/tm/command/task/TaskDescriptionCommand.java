package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "task-description";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show selected task description.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        terminalService.writeMessage("[TASK-DESCRIPTION]");
        String name = terminalService.getAnswer("Please enter project name: ");
        @Nullable final Project project = projectService.findOne(name, user.getId());
        @NotNull final String projectId = (project == null) ? "" : project.getId();
        name = terminalService.getAnswer("Please enter task name: ");
        @Nullable final Task task = taskService.findOne(name, projectId, user.getId());
        if (task == null) {
            terminalService.writeMessage("Selected task does not exist.");
            return;
        }
        terminalService.writeMessage(InfoUtil.getItemInfo(task));
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
