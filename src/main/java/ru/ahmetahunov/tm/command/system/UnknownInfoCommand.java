package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class UnknownInfoCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull String getName() {
        return "unknown";
    }

    @Override
    public @NotNull String getDescription() {
        return "Notifies about unknown operation.";
    }

    @Override
    public void execute() {
        serviceLocator.getTerminalService().writeMessage("Unknown operation. Please enter help for help.");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[0];
    }

}
