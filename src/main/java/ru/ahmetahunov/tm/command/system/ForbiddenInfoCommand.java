package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class ForbiddenInfoCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull String getName() {
        return "forbidden";
    }

    @Override
    public @NotNull String getDescription() {
        return "Notifies about low level access rights.";
    }

    @Override
    public void execute() {
        serviceLocator.getTerminalService().writeMessage("You don't have permission. Operation rejected");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[0];
    }

}
