package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return true; }

    @Override
    public @NotNull String getName() {
        return "exit";
    }

    @Override
    public @NotNull String getDescription() {
        return "Exit from Task Manager.";
    }

    public void execute() {
        serviceLocator.getTerminalService().writeMessage("Have a nice day!");
    }

}