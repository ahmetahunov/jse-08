package ru.ahmetahunov.tm.command;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    @Setter
    protected ServiceLocator serviceLocator;

    public abstract boolean isSecure();

    public abstract @NotNull String getName();

    public abstract @NotNull String getDescription();

    public abstract void execute() throws Exception;

    public @Nullable Role[] getRoles() {
        return null;
    }

}