package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.util.PassUtil;

@NoArgsConstructor
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "log-in";
    }

    @Override
    public @NotNull String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[LOG IN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @Nullable final User user = serviceLocator.getUserService().findUser(login);
        if (user == null) {
            terminalService.writeMessage("Selected user does not exist.");
            return;
        }
        @NotNull final String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String passHash = PassUtil.getHash(password);
        if (!user.getPassword().equals(passHash)) {
            terminalService.writeMessage("Wrong password.");
            return;
        }
        serviceLocator.getStateService().setCurrentUser(user);
        terminalService.writeMessage("Welcome, " + user.getLogin());
    }

}
