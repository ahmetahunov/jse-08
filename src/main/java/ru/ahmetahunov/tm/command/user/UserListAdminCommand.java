package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class UserListAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "user-list";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[USER LIST]");
        int i = 1;
        for (@NotNull User user : serviceLocator.getUserService().findAll()) {
            terminalService.writeMessage(String.format("%d. %s", i++, user.getLogin()));
        }
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
