package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.InfoUtil;

@NoArgsConstructor
public final class UserProfileAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "show-user-profile";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show selected user's profile.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.writeMessage("[USER PROFILE]");
        @NotNull final String login = terminalService.getAnswer("Please enter user login: ");
        @Nullable final User user = userService.findUser(login);
        if (user == null) {
            terminalService.writeMessage("Selected user does not exist.");
            return;
        }
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
