package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.PassUtil;

@NoArgsConstructor
public final class UserChangePassCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "change-pass";
    }

    @Override
    public @NotNull String getDescription() {
        return "Change password.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[CHANGE PASSWORD]");
        @NotNull String password = terminalService.getAnswer("Please enter old password: ");
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) {
            terminalService.writeMessage("Wrong password!");
            return;
        }
        password = terminalService.getAnswer("Please enter new password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass)) {
            terminalService.writeMessage("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        user.setPassword(password);
        serviceLocator.getUserService().merge(user);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
