package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
public final class UserRemoveAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "user-remove";
    }

    @Override
    public @NotNull String getDescription() {
        return "Remove selected user and all his projects and tasks.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.writeMessage("[USER REMOVE]");
        @NotNull String login = terminalService.getAnswer("Please enter user login: ");
        @Nullable final User user = userService.findUser(login);
        if (user == null) {
            terminalService.writeMessage("Selected user does not exist.");
            return;
        }
        taskService.removeAll(user.getId());
        projectService.removeAll(user.getId());
        userService.remove(user);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
