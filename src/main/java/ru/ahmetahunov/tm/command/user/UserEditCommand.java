package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.PassUtil;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "user-edit";
    }

    @Override
    public @NotNull String getDescription() {
        return "Edit user's information.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[USER EDIT]");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        password = PassUtil.getHash(password);
        if (!user.getPassword().equals(password)) {
            terminalService.writeMessage("Wrong password!");
            return;
        }
        @NotNull final String login = terminalService.getAnswer("Please enter new login: ");
        @NotNull final User checkUser = userService.findUser(login);
        if (checkUser != null) {
            terminalService.writeMessage("User with this login already exists!");
            return;
        }
        user.setLogin(login);
        serviceLocator.getUserService().merge(user);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
