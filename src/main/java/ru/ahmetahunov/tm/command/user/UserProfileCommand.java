package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.InfoUtil;

@NoArgsConstructor
public final class UserProfileCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @Override
    public @NotNull String getName() {
        return "user-profile";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show user's profile information.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final User user = serviceLocator.getStateService().getCurrentUser();
        if (user == null) return;
        terminalService.writeMessage("[USER PROFILE]");
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
