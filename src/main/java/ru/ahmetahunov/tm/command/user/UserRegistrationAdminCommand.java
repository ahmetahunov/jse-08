package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.util.PassUtil;
import ru.ahmetahunov.tm.util.RoleUtil;

@NoArgsConstructor
public final class UserRegistrationAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull String getName() {
        return "user-register-admin";
    }

    @Override
    public @NotNull String getDescription() {
        return "New user registration with role setting.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[REGISTRATION]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass)) {
            terminalService.writeMessage("Passwords do not match!");
            return;
        }
        password = PassUtil.getHash(password);
        @Nullable final User user = serviceLocator.getUserService().createNewUser(login, password);
        if (user == null) {
            terminalService.writeMessage("This login already exists.");
            return;
        }
        @NotNull String answer = terminalService.getAnswer("Please enter role<User/Administrator>: ");
        @Nullable final Role role = RoleUtil.getRole(answer);
        if (role == null) terminalService.writeMessage("Unknown role. Default role is set.");
        else user.setRole(role);
        terminalService.writeMessage("[OK]");
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}
