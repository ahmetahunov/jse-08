package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    @Getter
    private final IStateService stateService = new StateService(commands);

    @NotNull
    @Getter
    private final ITerminalService terminalService = new TerminalService(reader);

    public void init(@NotNull final Class[] classes) throws Exception {
        initCommands(classes);
        createDefaultUsers();
        startCycle();
    }

    private void initCommands(@NotNull final Class[] classes) throws Exception {
        for (@NotNull Class commandClass : classes) {
            if (!AbstractCommand.class.isAssignableFrom(commandClass)) continue;
            @NotNull final AbstractCommand command = (AbstractCommand) commandClass.newInstance();
            command.setServiceLocator(this);
            commands.put(command.getName(), command);
        }
    }

    private void startCycle() throws Exception {
        terminalService.writeMessage( "*** WELCOME TO TASK MANAGER ***" );
        @NotNull String operation = "";
        while (!"exit".equals(operation)) {
            operation = terminalService.getAnswer("Please enter command: ").toLowerCase();
            @Nullable final AbstractCommand command = stateService.getCommand(operation);
            if (command == null) continue;
            command.execute();
            terminalService.writeMessage("");
        }
        terminalService.close();
    }

    private void createDefaultUsers() throws NoSuchAlgorithmException {
        @NotNull User user = new User();
        user.setLogin("user");
        user.setPassword(PassUtil.getHash("0000"));
        userService.persist(user);
        user = new User();
        user.setLogin("admin");
        user.setPassword(PassUtil.getHash("admin"));
        user.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
    }

}
