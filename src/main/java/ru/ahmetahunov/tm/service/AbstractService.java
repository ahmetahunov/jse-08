package ru.ahmetahunov.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IAbstractService;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.api.repository.IRepository;
import java.util.UUID;

@RequiredArgsConstructor
public class AbstractService<T extends AbstractEntity> implements IAbstractService<T> {

    @NotNull
    private final IRepository<T> repository;

    @Override
    public @Nullable T persist(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        try {
            repository.persist(item);
            return item;
        } catch (IdCollisionException e) {
            item.setId(UUID.randomUUID().toString());
            return persist(item);
        }
    }

    @Override
    public @Nullable T merge(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        return repository.merge(item);
    }

    @Override
    public @Nullable T remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.remove(id);
    }

    @Override
    public @Nullable T remove(@Nullable final T item) {
        if (item == null) return null;
        if (item.getId().isEmpty()) return null;
        return repository.remove(item.getId());
    }

    @Override
    public @Nullable T findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOne(id);
    }

}
