package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository repository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.repository = projectRepository;
    }

    @Override
    public @Nullable Project createNewProject(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (repository.contains(projectName, userId)) return null;
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        return persist(project);
    }

    @Override
    public @Nullable Project findOne(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(name, userId);
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return repository.findAll(userId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

}
