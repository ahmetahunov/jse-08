package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository repository;

    public UserService(@NotNull final IUserRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public @Nullable User findUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return repository.findUser(login);
    }

    @Override
    public @NotNull Collection<User> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable User createNewUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (repository.contains(login)) return null;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        return persist(user);
    }

}
