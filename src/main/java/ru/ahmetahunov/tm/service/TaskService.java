package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Task;
import java.util.LinkedList;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public @Nullable Task createNewTask(@Nullable final String taskName, @Nullable final String projectId,
                              @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null) return null;
        if (repository.contains(taskName, projectId, userId)) return null;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return persist(task);
    }

    @Override
    public @NotNull List<Task> findAll(@Nullable final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        if (userId == null || userId.isEmpty()) return tasks;
        return repository.findAll(userId);
    }

    @Override
    public @NotNull List<Task> findAll(@Nullable final String projectId, @Nullable final String userId) {
        @NotNull final List<Task> tasks = new LinkedList<>();
        if (projectId == null || projectId.isEmpty()) return tasks;
        if (userId == null || userId.isEmpty()) return tasks;
        return repository.findAll(projectId, userId);
    }

    @Override
    public @Nullable Task findOne(@Nullable final String name, @Nullable final String projectId,
                        @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null) return null;
        return repository.findOne(name, projectId, userId);
    }

    @Override
    public void removeAllProjectTasks(@Nullable final String projectId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        for (@NotNull Task task : repository.findAll(projectId, userId)) {
            repository.remove(task.getId());
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.removeAll(userId);
    }

}
