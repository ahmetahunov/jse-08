package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.enumerated.Role;

@NoArgsConstructor
@Getter
@Setter
public final class User extends AbstractEntity {

    @NotNull
    private String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private Role role = Role.USER;

}
