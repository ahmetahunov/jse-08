package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.entity.IItem;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public final class Project extends AbstractEntity implements IItem {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = new Date(0);

    @NotNull
    private Date finishDate = new Date(0);

    @NotNull
    private String userId = "";

}
