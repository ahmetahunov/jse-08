package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntity {

    @NotNull
    protected String id = UUID.randomUUID().toString();

}
