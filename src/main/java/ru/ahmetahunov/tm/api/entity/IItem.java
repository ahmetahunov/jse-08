package ru.ahmetahunov.tm.api.entity;

import java.util.Date;

public interface IItem {

    public String getName();

    public String getDescription();

    public Date getStartDate();

    public Date getFinishDate();

}
