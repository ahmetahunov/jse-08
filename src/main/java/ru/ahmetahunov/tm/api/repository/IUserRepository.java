package ru.ahmetahunov.tm.api.repository;

import ru.ahmetahunov.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    public User findUser(String login);

    public void removeAll();

    public boolean contains(String login);

}
