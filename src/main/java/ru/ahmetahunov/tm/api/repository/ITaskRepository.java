package ru.ahmetahunov.tm.api.repository;

import ru.ahmetahunov.tm.entity.Task;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    public void removeAll(String userId);

    public List<Task> findAll(String userId);

    public List<Task> findAll(String projectId, String userId);

    public Task findOne(String taskName, String projectId, String userId);

    public boolean contains(String taskName, String projectName, String userId);

}
