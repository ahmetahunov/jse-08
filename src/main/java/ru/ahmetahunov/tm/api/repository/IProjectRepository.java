package ru.ahmetahunov.tm.api.repository;

import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    public void removeAll(String userId);

    public List<Project> findAll(String userId);

    public Project findOne(String projectName, String userId);

    public boolean contains(String projectName, String userId);

}
