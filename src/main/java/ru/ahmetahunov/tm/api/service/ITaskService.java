package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.entity.Task;
import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    public Task createNewTask(String taskName, String projectId, String userId);

    public List<Task> findAll(String userId);

    public List<Task> findAll(String projectId, String userId);

    public Task findOne(String taskName, String projectId, String userId);

    public void removeAll(String userId);

    public void removeAllProjectTasks(String projectId, String userId);

}
