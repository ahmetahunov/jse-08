package ru.ahmetahunov.tm.api.service;

import java.io.IOException;

public interface ITerminalService {

    public String readMessage() throws IOException;

    public void writeMessage(String message);

    public String getAnswer(final String question) throws IOException;

    public void close() throws IOException;

}
