package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.entity.AbstractEntity;

public interface IAbstractService<T extends AbstractEntity> {

    public T persist(T item);

    public T merge(T item);

    public T remove(String id);

    public T remove(T item);

    public T findOne(String id);

}
