package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.entity.User;
import java.util.Collection;

public interface IUserService extends IAbstractService<User> {

    public User findUser(String login);

    public Collection<User> findAll();

    public User createNewUser(String login, String password);

}
