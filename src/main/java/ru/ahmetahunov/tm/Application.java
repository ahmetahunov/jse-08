package ru.ahmetahunov.tm;

import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.command.project.*;
import ru.ahmetahunov.tm.command.system.*;
import ru.ahmetahunov.tm.command.task.*;
import ru.ahmetahunov.tm.command.user.*;
import ru.ahmetahunov.tm.context.Bootstrap;

public final class Application {

    @NotNull
    private final static Class[] CLASSES = {
            AboutCommand.class, ExitCommand.class, HelpCommand.class, ForbiddenInfoCommand.class,
            UnknownInfoCommand.class, ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectDescriptionCommand.class, ProjectEditDatesCommand.class, ProjectEditDescriptionCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class, ProjectSelectCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskDescriptionCommand.class, TaskEditDateCommand.class,
            TaskEditDescriptionCommand.class, TaskListCommand.class, TaskMoveCommand.class, TaskRemoveCommand.class,
            UserChangeAccessAdminCommand.class, UserChangePassAdminCommand.class, UserChangePassCommand.class,
            UserEditCommand.class, UserListAdminCommand.class, UserLogInCommand.class, UserLogOutCommand.class,
            UserProfileAdminCommand.class, UserProfileCommand.class, UserRegistrationAdminCommand.class,
            UserRegistrationCommand.class, UserRemoveAdminCommand.class
    };

    public static void main( final String[] args ) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}
