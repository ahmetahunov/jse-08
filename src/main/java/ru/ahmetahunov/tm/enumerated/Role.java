package ru.ahmetahunov.tm.enumerated;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Role {

    USER("User"),
    ADMINISTRATOR("Administrator");

    @NotNull
    private final String displayName;

    @NotNull
    public String displayName() {
        return displayName;
    }

}
